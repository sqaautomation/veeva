package com.cognizant.framework.selenium;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.TimeoutException;

/**
 * Class containing useful WebDriver utility functions
 * @author Cognizant
 */
public class WebDriverUtil {
	private CraftDriver driver;
	
	
	
	/**
	 * Constructor to initialize the {@link WebDriverUtil} object
	 * @param driver The {@link WebDriver} object
	 */
	public WebDriverUtil(CraftDriver driver) {
		this.driver = driver;
	}
	
	/**
	 * Function to pause the execution for the specified time period
	 * @param milliSeconds The wait time in milliseconds
	 */
	public void waitFor(long milliSeconds) {
		try {
			Thread.sleep(milliSeconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
	}
	
	/**
	 * Function to wait until the page loads completely
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilPageLoaded(long timeOutInSeconds) {
		WebElement oldPage = driver.findElement(By.tagName("html"));
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
									.until(ExpectedConditions.stalenessOf(oldPage));
		
	}
	
	/**
	 * Function to wait until the page readyState equals 'complete'
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilPageReadyStateComplete(long timeOutInSeconds) {
		ExpectedCondition<Boolean> pageReadyStateComplete =
			new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver driver) {
	                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
	            }
	        };
		    
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds)).until(pageReadyStateComplete);
	}
	
	/**
	 * Function to wait until the specified element is located
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementLocated(By by, long timeOutInSeconds) {
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
							.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
	/**
	 * Function to wait until the specified element is visible
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(By by, long timeOutInSeconds) {
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
							.until(ExpectedConditions.visibilityOfElementLocated(by));
	}
	
	/**
	 * Function to wait until the specified element is enabled
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementEnabled(By by, long timeOutInSeconds) {
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
							.until(ExpectedConditions.elementToBeClickable(by));
	}
	
	public void isalertpresentcheck(long timeOutInSeconds) {
		if((new WebDriverWait(driver.getWebDriver(), timeOutInSeconds)).until(ExpectedConditions.alertIsPresent()) != null)
		{
			driver.getWebDriver().switchTo().alert().accept();
		}
	}
	
	/**
	 * Function to wait until the specified element is enabled and to click on it
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementEnabledAndClick(By by, long timeOutInSeconds) {
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
							.until(ExpectedConditions.elementToBeClickable(by)).click();
	}
	
	
	/**
	 * Function to wait until the specified element is displayed
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public boolean waitUntilElementDisplayed(By by, long timeOutInSeconds) {
		return (new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
							.until(ExpectedConditions.elementToBeClickable(by)).isDisplayed();
	}
	
	/**
	 * Function to wait until the specified element is disabled
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementDisabled(By by, long timeOutInSeconds) {
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
			.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(by)));
	}
	
	/**
	 * Function to select the specified value from a listbox
	 * @param by The {@link WebDriver} locator used to identify the listbox
	 * @param item The value to be selected within the listbox
	 */
	public void selectListItem(By by, String item) {
		Select dropDownList = new Select(driver.findElement(by));
		dropDownList.selectByVisibleText(item);
	}
	
	/**
	 * Function to do a mouseover on top of the specified element
	 * @param by The {@link WebDriver} locator used to identify the element
	 */
	public void mouseOver(By by) {
		Actions actions = new Actions(driver.getWebDriver());
		actions.moveToElement(driver.findElement(by)).build().perform();
	}
	
	/**
	 * Function to verify whether the specified object exists within the current page
	 * @param by The {@link WebDriver} locator used to identify the element
	 * @return Boolean value indicating whether the specified object exists
	 */
	public Boolean objectExists(By by) {
		return !driver.findElements(by).isEmpty();
	}
	
	/**
	 * Function to verify whether the specified text is present within the current page
	 * @param textPattern The text to be verified
	 * @return Boolean value indicating whether the specified test is present
	 */
	public Boolean isTextPresent(String textPattern) {
		return driver.findElement(By.cssSelector("BODY")).getText().matches(textPattern);
	}
	
	/**
	 * Function to check if an alert is present on the current page
	 * @param timeOutInSeconds The number of seconds to wait while checking for the alert
	 * @return Boolean value indicating whether an alert is present
	 */
	public Boolean isAlertPresent(long timeOutInSeconds) {
		try {
			new WebDriverWait(driver.getWebDriver(), timeOutInSeconds).until(ExpectedConditions.alertIsPresent());
			return true;
		} catch (TimeoutException ex) {
			return false;
		}
	}
	
	public void waitTime(int wait) throws InterruptedException{
		Thread.sleep(wait);
	}
public void selectUsingVisibleText(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByVisibleText(value);
		
	}
	
	/**
	 * Function is used to get all the options for the dropdown field
	 * @author Tabasum
	 * @return 
	 */
	
	public List<WebElement> getSelectedOption(By by, long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		List<WebElement> options = oselectlistboxvalue.getOptions();
		return options;
		
	}
	
	
	/**
	 * Function is used to select dropdown using value attribute
	 * @author Tabasum
	 */
	
	public void selectUsingValue(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByValue(value);
	
	}
	
	/**
	 * Function is used to get the selected value from dropdown
	 * @author Tabasum
	 */
	
	public String getSelectedListItem(By by) {

		Select select = new Select(driver.findElement(by));
		String option = select.getFirstSelectedOption().getText();
		return option;
	}
	
	/**
	 * Function is used to get the current date
	 * @author Tabasum
	 */
	public void deselectall(By by) {

		Select select = new Select(driver.findElement(by));
		select.deselectAll();
	}
	
public void selectUsingVisibleIndex(By by, long timeOutInSeconds, int n) {
		
		waitUntilElementEnabled(by, timeOutInSeconds);
		waitUntilElementVisible(by, timeOutInSeconds);
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByIndex(n);
		
	}
	public String getCurrentDate(){
		
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		Date date = new Date();
		String cur_Date = dateFormat.format(date).toString();
		return cur_Date;
	}
	
	/**
	 * Function is used to get the text from dropdown
	 * @author Tabasum
	 */
	
	public String getText(By by,long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.visibilityOfElementLocated(by));
		
		String text = driver.findElement(by).getText().trim();
		return text;
	}
	
	public String getValue(By by,long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.visibilityOfElementLocated(by));
		
		String value = driver.findElement(by).getAttribute("value").trim();
		return value;
	}
	
	public void enterText(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
		
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(value);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clickButton(By by, long timeOutInSeconds)
	{
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();	
	}

	public void clickLink(By by, long timeOutInSeconds){
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
	}
	
	public void clickMultiSelectListBox(By by, long timeOutInSeconds) {

		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
	}
	
	public void waitForGetResultSet(By by, long timeOutInSeconds){
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
	public boolean objectEnabled(By by, long timeOutInSeconds)
	{
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
				
		boolean statusenabled = driver.findElement(by).isEnabled();
		return statusenabled;
		
	}
	
	public boolean objectDisplayed(By by, long timeOutInSeconds)
	{
		return (new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
				.until(ExpectedConditions.elementToBeClickable(by)).isDisplayed();
	} 

}