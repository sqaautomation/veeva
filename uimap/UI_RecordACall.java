package uimap;

import org.openqa.selenium.By;

/**
 * UI Map for Apttus Login Page 
 */
public class UI_RecordACall {
	
	public static final By btnHome = By.xpath("//*[@id='home_Tab']");
	public static final By txtSearchBox = By.xpath("//input[@class='searchTextBox']");
	public static final By btnSearchGo = By.xpath("//input[@name='search']");
	
	public static final By lnkSearchUser = By.xpath("//*[@id='Account_body']/table/tbody/tr[2]/th/a");
	
	public static final By btnRecordCall = By.xpath("(//input[@name='record_a_call'])[1]");
	public static final By btnRecordGroupCall = By.xpath("(//input[@name='record_a_call'])[1]");
	
	public static final By btnSubmit = By.xpath("//*[@id='topButtonRow']/span[3]/input");
	public static final By btnSave = By.xpath("//*[@id='topButtonRow']/span[1]/input");
	//public static final By chkboxDetailingPriority = By.xpath("//*[@id='chkd_a001k000001acleAAA']");
	//public static final By chkboxKeyFocusAreas = By.xpath("//*[@id='a001k000001acleAAA']");	
	//public static final By chkboxDetailingPriority = By.xpath("//*[@id='chkd_a00L000000ARSkTIAX']");
	public static final By chkboxDetailingPriority = By.xpath("//*[contains(@id='chkd_a00L')]");
	public static final By chkboxKeyFocusAreas = By.xpath("//*[@id='a00L000000ARSkTIAX']");
	public static final By chkboxOfferedSample = By.xpath("//*[@id='PCYC_HCP_offered_samples__c']");	
	public static final By chkboxRequestForCLL = By.xpath("//*[@id='PCYC_HCP_requested_for_CLL__c']");
	public static final By chkboxRefuseForCLL = By.xpath("//*[@id='PCYC_HCP_refused_the_sample__c']");
	public static final By chkboxRequestForMCL = By.xpath("//*[@id='PCYC_Prescriber_requested_for_MCL__c']");
	public static final By chkboxRefuseForMCL = By.xpath("//*[@id='PCYC_Prescriber_refused_MCL__c']");
	public static final By chkboxComplianceAffirm = By.xpath("//*[@id='PCYC_Compliance_Affirm__c']");
	public static final By chkboxLeaveBehinds = By.xpath("//*[@id='chka000W00000qtAZUQA2']");
	
	public static final By txtCheckStatus = By.xpath("//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[2]/div[2]/span");
	public static final By txtCheckGroupStatus = By.xpath("//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[2]/div[2]/span/span/span/veev-field/span/span");
	public static final By txtPrescriberStatus = By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]");
	
	public static final By lnkPrescriberName = By.xpath("//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[1]/div[2]/span/span/span/veev-field/span/div/a");
	
	public static final By chkboxAttendees1 = By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[3]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/veev-field/span/div/div[1]/attendee-checkbox/table/tbody/tr/td[1]/label/table/tbody/tr[1]/td[1]/input");
	public static final By chkboxAttendees2 = By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[3]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/veev-field/span/div/div[1]/attendee-checkbox/table/tbody/tr/td[2]/label/table/tbody/tr[1]/td[1]/input");
	
	public static final By lnkAttendees1 = By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[3]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/veev-field/span/div/div[2]/div[2]/div[1]/a");
	public static final By lnkAttendees2 = By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[3]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/veev-field/span/div/div[2]/div[3]/div[1]/a");
	
	
	public static final By errormsgDisplay = By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/div/div");
	
	public static final By lnkMedicalCenter = By.xpath("//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[1]/div[2]/span/span/span/veev-field/span/div/a");
	
	public static final By callNumber = By.xpath("//*[@id='001d0000023GFHC_00Nd00000078ls1_body']/table/tbody/tr[2]/th");
	public static final By callStatus = By.xpath("//*[@id='001d0000023GFHC_00Nd00000078ls1_body']/table/tbody/tr[2]/td[3]");
	
			
}