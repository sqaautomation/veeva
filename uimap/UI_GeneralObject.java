package uimap;

import org.openqa.selenium.By;

/**
 * UI Map for Apttus Login Page 
 */
public class UI_GeneralObject {

	//login inside application
	public static final By btnLogin_HomePage = By.xpath("//input[@name='login']");
	
	//loading image
	public static final By imgApttusPageLoading = By.xpath("//*[@id='loading-spinner']");
	
	//search user name, agreement number
	public static final By btnHome = By.xpath("//*[@id='home_Tab']");
	public static final By txtSearchBox = By.xpath("//input[@class='searchTextBox']");
	public static final By btnSearchGo = By.xpath("//input[@name='search']");
	public static final By lnkNoMatchingRecord = By.xpath("//strong[contains(text(),'There are no matching')]");
	public static final By lnkAgreementNumberSearchValue =By.xpath("//*[@id='Apttus__APTS_Agreement__c_body']/table/tbody/tr[2]/th/a");
		
	//agreement status
	public static final By txtAgreementCurrentStatus = By.xpath("//*[contains(@id,'XRPW')]");

	//search in new window
	public static final By txtNewWindowSearch =By.xpath("//*[@id='lksrch']");
	public static final By btnNewWindowSearchGo =By.xpath("//*[@id='theForm']/div/div[2]/input[2]");
	public static final By radiobtnNewWindownSearchAllFields =By.xpath("//*[@id='lkenhmdSEARCH_ALL']");	
	
	//search value selection
	public static final By tableUserBody = By.xpath("//*[@id='User_body']/table/tbody");
	public static final By lnkSearchValue_UserBody = By.xpath("//*[@id='User_body']/table/tbody/tr[2]/th/a");
	public static final By tableAccountBody = By.xpath("//*[@id='User_body']/table/tbody");
	public static final By lnkSearchValue_AccountBody = By.xpath("//*[@id='Account_body']/table/tbody/tr[2]/th/a");
	public static final By tableAgreementBody = By.xpath("//*[@id='Apttus__APTS_Agreement__c_body']/table/tbody");
	public static final By lnkSearchValue_AgreementBody = By.xpath("//*[@id='Apttus__APTS_Agreement__c_body']/table/tbody/tr[2]/th/a");
	
	//agreement details
	public static final By btnEdit=By.xpath("(//input[@name='edit'])[1]");
	public static final By btnSave=By.xpath("(//input[@value='Save'])[1]");
	
	//need to cluster later -- need to remove
	//public static final By btnSearchGo_AgreementNumber = By.xpath("//*[@id='sbsearch']/div[2]/div[1]/input[2]");
	
	//parent agreement search
	public static final By btnAgreementLookup = By.xpath("//img[contains(@title,'Agreement Lookup')]");
}