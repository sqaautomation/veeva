package uimap;

import org.openqa.selenium.By;

/**
 * UI Map for Apttus Login Page 
 */
public class UI_Login {
	
	//login
	public static final By txtUsername = By.xpath(".//*[@id='username']");
	public static final By txtPassword = By.xpath(".//*[@id='password']");		
	public static final By btnLogin = By.xpath(".//*[@id='Login']");
	
	//logout
	public static final By lnkUserName = By.xpath("//*[@id='userNavLabel']");
	public static final By btnLogout = By.xpath("//*[@id='userNav-menuItems']/a[2]");

}