package businesscomponents;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;

import com.cognizant.framework.Status;

import supportlibraries.*;
import uimap.UI_Login;

/**
 * Class for storing general purpose business components
 * 
 * @author Cognizant
 */
public class BC_Login extends ReusableLibrary {

	public static long timeOutInSeconds = 100;
	public static int lowseconds = 5000;
	

	/**
	 * Constructor to initialize the component library
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	public BC_Login(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public void invokeApplication() throws InterruptedException {		
		driver.manage().deleteAllCookies();
		driver.get(properties.getProperty("ApplicationUrl"));
		// waitTime(seconds);
	}

	public void login() throws InterruptedException, IOException, AWTException {

		String tempUserName = dataTable.getData("General_Data", "UserName").trim();
		String userName = properties.getProperty(tempUserName).trim();
		String password = properties.getProperty(tempUserName + "Pwd").trim();

		report.updateTestLog("Enter user credentials",
				"Specify " + "username = " + userName + ", " + "password = " + password, Status.PASS);

		driverUtil.enterText(UI_Login.txtUsername, timeOutInSeconds, userName);

		driverUtil.enterText(UI_Login.txtPassword, timeOutInSeconds, password);
	
		driverUtil.clickButton(UI_Login.btnLogin, timeOutInSeconds);
	
		report.updateTestLog("Login", "Click the 'Log In to Sandbox' button", Status.PASS);

	}

	public void logout() throws InterruptedException {
		System.out.println("Successfully completed the test case");
		driverUtil.clickLink(UI_Login.lnkUserName, timeOutInSeconds);
		driverUtil.clickButton(UI_Login.btnLogout, timeOutInSeconds);	
	}
	
	/**
	 * @Method closeAllBrowsers
	 * @author Cognizant
	 * @param none
	 * @return none
	 * @throws InterruptedException
	 * @date 13-Mar-2018
	 */
	public void closeAllBrowsers() throws NoSuchElementException, Throwable {
		driver.quit();
	}

}