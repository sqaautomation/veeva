package businesscomponents;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.DriverScript;
import supportlibraries.ReusableLibrary;
import supportlibraries.ReusableWebDriverLibrary;
import supportlibraries.ScriptHelper;
import uimap.UI_GeneralObject;
import uimap.UI_RecordACall;

/**
 * Class for storing general purpose business components
 * 
 * @author Cognizant
 */
public class BC_RecordACall extends ReusableLibrary {

	public static long timeOutInSeconds = 100;
	public static int lowseconds = 5000;
	BC_Login loginclass = new BC_Login(scriptHelper);

	/**
	 * Constructor to initialize the component library
	 * 
	 * @param scriptHelper
	 *            The {@link ScriptHelper} object passed from the
	 *            {@link DriverScript}
	 */
	public BC_RecordACall(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	/**
	 * @Method verifyPrescriber
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyPrescriber() throws NoSuchElementException, Throwable {
		String prescriberName = dataTable.getData("General_Data", "PrescriberName").trim();
		searchForUser(prescriberName);
		
		WebElement prescriberLink = driver.findElement(By.xpath("//*[@id='Account_body']/table/tbody/tr[2]/th/a"));
		
		if (prescriberLink.getText().trim().equalsIgnoreCase(prescriberName)) {
			report.updateTestLog("Prescriber Account Records present for Sales Rep",
					"As a Sales rep HCP Prescriber records are displayed based on search result", Status.PASS);
		}
		
		else {
			report.updateTestLog("Prescriber Account Records present for Sales Rep",
					"As a Sales rep HCP Prescriber records are not displayed based on search result", Status.FAIL);
		}
		
		driverUtil.clickLink(UI_RecordACall.lnkSearchUser, timeOutInSeconds);
		driverUtil.waitFor(4000);

		WebElement statusCheck = driver.findElement(By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]"));
		String dataToVerify_PrescriberStatus = statusCheck.getAttribute("outerHTML");
		if (dataToVerify_PrescriberStatus.contains("Prescriber Account")) {
			report.updateTestLog("Prescriber Account Detail View as Sales Rep",
					"Status of newly selected Prescriber is matching as 'Prescriber Account'", Status.PASS);
		} else {
			report.updateTestLog("Prescriber Account Detail View as Sales Rep",
					"Status of newly selected Prescriber is not matching as 'Prescriber Account'", Status.FAIL);
		}
		
	}	
	
	/**
	 * @Method verifyRecordACallButton
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyRecordACallButton() throws NoSuchElementException, Throwable {
		String prescriberName = dataTable.getData("General_Data", "PrescriberName").trim();
		searchForUser(prescriberName);
		driverUtil.clickLink(UI_RecordACall.lnkSearchUser, timeOutInSeconds);
		driverUtil.waitFor(4000);
				
		if(driverUtil.objectEnabled(UI_RecordACall.btnRecordCall, 2000)) {
			report.updateTestLog("Record a call button",
					"Record a call button is enabled", Status.PASS);

			driverUtil.clickButton(UI_RecordACall.btnRecordCall, timeOutInSeconds);
			driverUtil.waitFor(8000);
		}
		else
			report.updateTestLog("Record a call button",
					"Record a call button is disabled", Status.FAIL);
	}	
	
	
	/**
	 * @Method verifyCallReportView
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyCallReportView() throws NoSuchElementException, Throwable {
		
		verifyRecordACallButton();
		
		WebElement callReport = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/div[2]/div[2]/div/h2"));
		
		if(callReport.getText().trim().contains("New Call Report")) {
			report.updateTestLog("Verify call report view",
					"On clicking Record a Call Button, application navigates to new call report page", Status.PASS);
		}
		else{
			report.updateTestLog("Verify call report view",
					"On clicking Record a Call Button, application doesnot navigates to new call report page", Status.FAIL);
		}
		
	}	
	
	
	/**
	 * @Method prepopulatedFields
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void prepopulatedFields() throws NoSuchElementException, Throwable {
		
		verifyRecordACallButton();
		
		WebElement account = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[1]/span/div[1]/div[1]/label"));		
		verifyPrepopulatedFields(account,"Account");
		
		WebElement address = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[5]/span/div[1]/div[1]/label"));		
		verifyPrepopulatedFields(address,"Address");
		
		WebElement siginSheet = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[6]/span/div[1]/div[1]/label"));		
		verifyPrepopulatedFields(siginSheet,"Sign-In Sheet");
		
		WebElement dateTime = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[2]/span/div[2]/div[1]/label"));		
		verifyPrepopulatedFields(dateTime,"Datetime");
		
		WebElement recordType = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[4]/span/div[2]/div[1]/label"));		
		verifyPrepopulatedFields(recordType,"Record Type");
		
		WebElement callCategory = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[1]/div/div/div/div[2]/div[5]/span/div[2]/div[1]/label"));		
		verifyPrepopulatedFields(callCategory,"Call Category");
		
	}
	
	/**
	 * @Method verifyPrepopulatedFields
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyPrepopulatedFields(WebElement fieldValue, String fieldName) throws NoSuchElementException, Throwable {
		
		if(fieldValue.getText().trim().equalsIgnoreCase(fieldName)) {
			report.updateTestLog("Verify Pre-populated fields: " + fieldName,
					"Pre populated field : '" + fieldName + "' is available in the Professional Information Section", Status.PASS);
		}
		else{
			report.updateTestLog("Verify Pre-populated fields: " + fieldName,
					"Pre populated field : '" + fieldName + "' is not available in the Professional Information Section", Status.FAIL);
		}
		
	}

	
	/**
	 * @Method detailingPriorityFields
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void detailingPriorityFields() throws NoSuchElementException, Throwable {
		
		verifyRecordACallButton();
		
		WebElement unbrandedCLL = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[1]/label"));		
		verifyPrepopulatedFields(unbrandedCLL,"UNBRANDED CLL");
		
		WebElement imbruvica1lCLL = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[2]/label"));		
		verifyPrepopulatedFields(imbruvica1lCLL,"IMBRUVICA 1L CLL");
		
		WebElement imbruvica2lCLL = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[3]/label"));
		verifyPrepopulatedFields(imbruvica2lCLL,"IMBRUVICA 2L CLL");
		
		WebElement imbruvicaWM = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[4]/label"));
		verifyPrepopulatedFields(imbruvicaWM,"IMBRUVICA WM");
		
		WebElement imbruvicaMCL = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[5]/label"));
		verifyPrepopulatedFields(imbruvicaMCL,"IMBRUVICA MCL");
		
		WebElement imbruvicacGVHD = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[1]/td[6]/label"));
		verifyPrepopulatedFields(imbruvicacGVHD,"IMBRUVICA cGVHD");
		
		WebElement imbruvicaMZL = driver.findElement(By.xpath("//*[@id='vod_detailing']/tbody/tr[2]/td/label"));
		verifyPrepopulatedFields(imbruvicaMZL,"IMBRUVICA MZL");
		
	}
	
	/**
	 * @Method VerifyDetailingPriorityFields
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void VerifyDetailingPriorityFields(WebElement fieldValue, String fieldName) throws NoSuchElementException, Throwable {
		
		boolean booValue = fieldValue.isEnabled();
		
		if(booValue) {
			report.updateTestLog("Verify Editable fields in Detailing Priority Section: " + fieldName,
					"Detailing Priority " + fieldName + " is editable checkbox", Status.PASS);
		}
		else{
			report.updateTestLog("Verify Editable fields in Detailing Priority Section: " + fieldName,
					"Detailing Priority " + fieldName + " is non-editable checkbox", Status.FAIL);
		}
		
	}
	
	
	/**
	 * @Method verifyKeyFocusArea
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyKeyFocusArea() throws NoSuchElementException, Throwable {
		verifyRecordACallButton();
		
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Key Focus Area Verification",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Key Focus Area Verification",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.FAIL);
		}
		
	}
	
	
	/**
	 * @Method createIndividualRecordCall
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void createIndividualRecordCall() throws NoSuchElementException, Throwable {
		String prescriberName = dataTable.getData("General_Data", "PrescriberName").trim();
		String requestedForCLL = dataTable.getData("General_Data", "RequestedForCLL").trim();
		String requestedForMCL = dataTable.getData("General_Data", "RequestedForMCL").trim();
		verifyPrescriber();
		WebElement statusCheck = driver.findElement(By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]"));
		
		//Updated to remove the duplicate search
		if(driverUtil.objectEnabled(UI_RecordACall.btnRecordCall, 2000)) {
			report.updateTestLog("Record a call button",
					"Record a call button is enabled", Status.PASS);

			driverUtil.clickButton(UI_RecordACall.btnRecordCall, timeOutInSeconds);
			driverUtil.waitFor(8000);
		}
		else
			report.updateTestLog("Record a call button",
					"Record a call button is disabled", Status.FAIL);
		
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxOfferedSample, timeOutInSeconds);
		driverUtil.waitFor(2000);

		if (requestedForCLL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

		if (requestedForMCL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxComplianceAffirm, timeOutInSeconds);
		driverUtil.waitFor(2000);
		
		//Not required for QA Sandbox
		//driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxLeaveBehinds, timeOutInSeconds);
		driverUtil.waitFor(2000);

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(10000);

		
		WebElement callNumber = driver
				.findElement(By.xpath("//*[@id='veeva-app']/div/div/div/div[2]/div[1]/div/h2"));
		dataTable.putData("General_Data", "CallNumber", callNumber.getText().trim());
		report.updateTestLog("Record Call Creation",
				"Successfully created the individual record call with the call number as " + callNumber.getText().trim(), Status.PASS);
		
		
		statusCheck = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[2]/div[2]/span"));
		String dataToVerify_RecordStatus = statusCheck.getAttribute("outerHTML");

		if (dataToVerify_RecordStatus.contains("Submitted")) {
			report.updateTestLog("Record a Call", "Status is matched for newly created Record a call as Submitted",
					Status.PASS);
		} else {
			report.updateTestLog("Record a Call", "Status is not matched for newly created Record a call as Submitted",
					Status.FAIL);
		}

		WebElement primaryAddress = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[5]/span/div[1]/div[2]/span/span/span/veev-field/span/span"));
		String dataToVerify_PrimaryAddress = primaryAddress.getText();

		dataTable.putData("General_Data", "PrimaryAddress", dataToVerify_PrimaryAddress);

		driverUtil.clickLink(UI_RecordACall.lnkPrescriberName, timeOutInSeconds);

		WebElement Address = driver
				.findElement(By.xpath("//*[@id='001d000001nn1hK_00Nd00000078lrD_body']/table/tbody/tr[2]/th"));
		String dataToVerify_Address = Address.getText();

		if (dataToVerify_Address.contains(dataToVerify_PrimaryAddress.split(",", 2)[0])) {
			report.updateTestLog("Primary Address Verification",
					"Primary address is matched with the address created in the new Record Call", Status.PASS);
		} else {
			report.updateTestLog("Primary Address Verification",
					"Primary address is not matched with the address created in the new Record Call", Status.FAIL);
		}

	}

	
	/**
	 * @Method verifyIndividualRecordCall
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyIndividualRecordCall() throws NoSuchElementException, Throwable {
		String prescriberName = dataTable.getData("General_Data", "PrescriberName").trim();
		String requestedForCLL = dataTable.getData("General_Data", "RequestedForCLL").trim();
		String requestedForMCL = dataTable.getData("General_Data", "RequestedForMCL").trim();
		verifyPrescriber();
		WebElement statusCheck = driver.findElement(By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]"));		
		verifyRecordACallButton();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxOfferedSample, timeOutInSeconds);
		driverUtil.waitFor(2000);
		
		String samplingStage = "Prescriber Offered";
		
		if (requestedForCLL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
			samplingStage = samplingStage + "Prescriber Requested CLL";
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
			samplingStage = samplingStage + "Prescriber Refused CLL";
		}

		if (requestedForMCL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
			samplingStage = samplingStage + "Prescriber Requested MCL";
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
			samplingStage = samplingStage + "Prescriber Refused MCL";
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxComplianceAffirm, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxLeaveBehinds, timeOutInSeconds);
		driverUtil.waitFor(2000);

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(10000);

		
		WebElement callNumber = driver
				.findElement(By.xpath("//*[@id='veeva-app']/div/div/div/div[2]/div[1]/div/h2"));
		dataTable.putData("General_Data", "CallNumber", callNumber.getText().trim());
		report.updateTestLog("Record Call Creation",
				"Successfully created the individual record call with the call number as " + callNumber.getText().trim(), Status.PASS);
		
		
		statusCheck = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[2]/div[2]/span"));
		String dataToVerify_RecordStatus = statusCheck.getAttribute("outerHTML");

		if (dataToVerify_RecordStatus.contains("Submitted")) {
			report.updateTestLog("Record a Call", "Status is matched for newly created Record a call as Submitted",
					Status.PASS);
		} else {
			report.updateTestLog("Record a Call", "Status is not matched for newly created Record a call as Submitted",
					Status.FAIL);
		}

		WebElement primaryAddress = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[5]/span/div[1]/div[2]/span/span/span/veev-field/span/span"));
		String dataToVerify_PrimaryAddress = primaryAddress.getText();

		dataTable.putData("General_Data", "PrimaryAddress", dataToVerify_PrimaryAddress);

		driverUtil.clickLink(UI_RecordACall.lnkPrescriberName, timeOutInSeconds);

		WebElement Address = driver
				.findElement(By.xpath("//*[@id='001d000001nn1hK_00Nd00000078lrD_body']/table/tbody/tr[2]/th"));
		String dataToVerify_Address = Address.getText();

		if (dataToVerify_Address.contains(dataToVerify_PrimaryAddress.split(",", 2)[0])) {
			report.updateTestLog("Primary Address Verification",
					"Primary address is matched with the address created in the new Record Call", Status.PASS);
		} else {
			report.updateTestLog("Primary Address Verification",
					"Primary address is not matched with the address created in the new Record Call", Status.FAIL);
		}

		
		WebElement finalStatus = driver.findElement(By.xpath(
				"//*[@id='001d000001nn1hK_00Nd00000078ls1_body']/table/tbody/tr[2]/td[3]"));
		String statusCheckPostCreation = finalStatus.getText().trim();
		
		if (statusCheckPostCreation.equalsIgnoreCase("Submitted")) {			
			report.updateTestLog("Status Verification - Calls (Account) section",
					"Status is matched for newly created Record a call as Submitted", Status.PASS);
		}
		else {
			report.updateTestLog("Status Verification - Calls (Account) section",
					"Status is not matched for newly created Record a call as Submitted", Status.FAIL);
		}
		
		
		WebElement prescriberOffered = driver.findElement(By.xpath(
				"//*[@id='001d000001nn1hK_00N1k000000Xmf8_body']/table/tbody/tr[2]/td[4]"));
		String prescriberOfferedValue = prescriberOffered.getText().trim();

		WebElement prescriberCLL = driver.findElement(By.xpath(
				"//*[@id='001d000001nn1hK_00N1k000000Xmf8_body']/table/tbody/tr[3]/td[4]"));
		String prescriberCLLValue = prescriberCLL.getText().trim();
		
		WebElement prescriberMCL = driver.findElement(By.xpath(
				"//*[@id='001d000001nn1hK_00N1k000000Xmf8_body']/table/tbody/tr[4]/td[4]"));
		String prescriberMCLValue = prescriberMCL.getText().trim();
		
		
		if(samplingStage.contains(prescriberOfferedValue)) {
			if(samplingStage.contains(prescriberCLLValue)) {
				if(samplingStage.contains(prescriberMCLValue)) {
					report.updateTestLog("Status Verification - Sampling Stage section",
							"Values are matched in Sampling Stage section as selected in the new record call ", Status.PASS);
				}
				else {
					report.updateTestLog("Status Verification - Sampling Stage section",
							"Values are not matched in Sampling Stage section as selected in the new record call ", Status.FAIL);
				}
			}
			else {
				report.updateTestLog("Status Verification - Sampling Stage section",
						"Values are not matched in Sampling Stage section as selected in the new record call ", Status.FAIL);
			}
		}
		else {
			report.updateTestLog("Status Verification - Sampling Stage section",
					"Values are not matched in Sampling Stage section as selected in the new record call ", Status.FAIL);
		}
	}
	
	
	/**
	 * @Method verifyErrorMsgSamplingStage
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyErrorMsgSamplingStage() throws NoSuchElementException, Throwable {
		verifyPrescriber();			
		verifyRecordACallButton();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxOfferedSample, timeOutInSeconds);
		driverUtil.waitFor(2000);


		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForCLL, timeOutInSeconds);
		driverUtil.waitFor(2000);

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		errorVerification("SamplingStageErrorMsg");
		driverUtil.waitFor(2000);
		
	}
	
	
	/**
	 * @Method verifyErrorMsgAffrim
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyErrorMsgAffrim() throws NoSuchElementException, Throwable {
		String requestedForCLL = dataTable.getData("General_Data", "RequestedForCLL").trim();
		String requestedForMCL = dataTable.getData("General_Data", "RequestedForMCL").trim();
		verifyPrescriber();
		WebElement statusCheck = driver.findElement(By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]"));		
		verifyRecordACallButton();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxOfferedSample, timeOutInSeconds);
		driverUtil.waitFor(2000);

		if (requestedForCLL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

		if (requestedForMCL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		errorVerification("AffirmErrorMsg");
		driverUtil.waitFor(2000);
		
	}
	
	
	/**
	 * @Method verifyErrorMsgCallReport
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyErrorMsgCallReport() throws NoSuchElementException, Throwable {

		verifyPrescriber();	
		verifyRecordACallButton();
		
		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		errorVerification("CallReportErrorMsg");
		driverUtil.waitFor(2000);
		
	}
	
	/**
	 * @Method verifyErrorMsgDetailingPriority
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyErrorMsgDetailingPriority() throws NoSuchElementException, Throwable {

		verifyPrescriber();
		WebElement statusCheck = driver.findElement(By.xpath("//*[@id='ep']/div[2]/div[2]/table/tbody/tr[1]/td[4]"));		
		verifyRecordACallButton();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		errorVerification("DetailingPriorityErrorMsg");
		driverUtil.waitFor(2000);
		
	}
	
	
	/**
	 * @Method verifyErrorMsgPrescriberOffer
	 * @author Cognizant
	 * @param none
	 * @return Agreement number
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void verifyErrorMsgPrescriberOffer() throws NoSuchElementException, Throwable {
		verifyPrescriber();			
		verifyRecordACallButton();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[4]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
		driverUtil.waitFor(2000);

		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		errorVerification("PrescriberOfferErrorMsg");
		driverUtil.waitFor(2000);
		
	}
	
	
	/**
	 * @Method createGroupRecordCall
	 * @author Cognizant
	 * @param none
	 * @return none
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void createGroupRecordCall() throws NoSuchElementException, Throwable {
		String medicalCenterName = dataTable.getData("General_Data", "MedicalCenterName").trim();

		String detailingPriorityErrorMsg = dataTable.getData("General_Data", "DetailingPriorityErrorMsg").trim();

		searchForUser(medicalCenterName);

		driverUtil.clickLink(UI_RecordACall.lnkSearchUser, timeOutInSeconds);
		driverUtil.waitFor(4000);

		driverUtil.clickButton(UI_RecordACall.btnRecordGroupCall, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxAttendees1, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxAttendees2, timeOutInSeconds);
		driverUtil.waitFor(2000);


		// negative verification - Call report view
		//driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(3000);

		// 1st Prescriber
		driverUtil.clickLink(UI_RecordACall.lnkAttendees1, timeOutInSeconds);
		driverUtil.waitFor(2000);

		//errorVerification("CallReportErrorMsg");
		callDetails("firstUser");
		driverUtil.clickButton(UI_RecordACall.btnSave, timeOutInSeconds);
		driverUtil.waitFor(4000);

		// 2nd Prescriber
		driverUtil.clickLink(UI_RecordACall.lnkAttendees2, timeOutInSeconds);
		driverUtil.waitFor(2000);

		//errorVerification("CallReportErrorMsg");
		callDetails("secondUser");
		driverUtil.clickButton(UI_RecordACall.btnSave, timeOutInSeconds);
		driverUtil.waitFor(4000);
		
		//Affirm Verification
		//driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		//errorVerification("AffirmErrorMsg");
		driverUtil.waitFor(2000);
		
		//Positive scenario continues
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxComplianceAffirm, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
		driverUtil.waitFor(8000);
		
		//Fetch Call number and update in the data sheet
		
		WebElement callNumber = driver
				.findElement(By.xpath("//*[@id='veeva-app']/div/div/div/div[2]/div[1]/div/h2"));
		dataTable.putData("General_Data", "CallNumber", callNumber.getText().trim());
		report.updateTestLog("Record Call Creation",
				"Successfully created the group record call with the call number as " + callNumber.getText().trim(), Status.PASS);
		
		WebElement groupStatus = driver
				.findElement(By.xpath("//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[1]/span/div[2]/div[2]/span/span/span/veev-field/span/span"));
		
		if(groupStatus.getText().equalsIgnoreCase("Submitted")) {
			
			report.updateTestLog("Record a Group Call", "Status is matched for newly created Record a call as Submitted",
					Status.PASS);
		} else {
			report.updateTestLog("Record a Group Call", "Status is not matched for newly created Record a call as Submitted",
					Status.FAIL);
		}

		
		//Verification
		WebElement primaryAddress = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/div/div[3]/div[3]/span[1]/div/div/div/div[2]/div[4]/span/div[1]/div[2]/span/span/span/veev-field/span/span"));
		String dataToVerify_PrimaryAddress = primaryAddress.getText();

		dataTable.putData("General_Data", "PrimaryAddress", dataToVerify_PrimaryAddress);

		driverUtil.clickLink(UI_RecordACall.lnkMedicalCenter, timeOutInSeconds);

		WebElement Address = driver
				.findElement(By.xpath("//*[@id='001d0000023GFHC_00Nd00000078lrD_body']/table/tbody/tr[2]/th"));
		String dataToVerify_Address = Address.getText();

		if (dataToVerify_Address.contains(dataToVerify_PrimaryAddress.split(",", 2)[0])) {
			report.updateTestLog("Primary Address Verification",
					"Primary address is matched with the address created in the new Record Call", Status.PASS);
		} else {
			report.updateTestLog("Primary Address Verification",
					"Primary address is not matched with the address created in the new Record Call", Status.FAIL);
		}
		
		
		
		
		/*WebElement callNumberValue = driver
				.findElement(By.xpath("//*[@id='001d0000023GFHC_00Nd00000078ls1_body']/table/tbody/tr[2]/th/a"));
		String callNumber_Verify = callNumberValue.getText();

		if (callNumber_Verify.contains(callNumber.getText().trim())) {
			report.updateTestLog("Primary Address Verification",
					"Primary address is matched with the address created in the new Record Call", Status.PASS);
		} else {
			report.updateTestLog("Primary Address Verification",
					"Primary address is not matched with the address created in the new Record Call", Status.FAIL);
		}*/
		
		
		WebElement callStatus = driver
				.findElement(By.xpath("//*[@id='001d0000023GFHC_00Nd00000078ls1_body']/table/tbody/tr[2]/td[3]"));
		String callStatus_Verify = callStatus.getText();

		if (callStatus_Verify.contains("Submitted")) {
			report.updateTestLog("Status verification in Account Details View",
					"Status is matched for newly created Record a call as Submitted", Status.PASS);
		} else {
			report.updateTestLog("Status verification in Account Details View",
					"Status is not matched for newly created Record a call as Submitted", Status.FAIL);
		}
	}

	/**
	 * @Method callDetails
	 * @author Cognizant
	 * @param none
	 * @return none
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void callDetails(String userType) throws NoSuchElementException, Throwable {
		String requestedForCLL = dataTable.getData("General_Data", "RequestedForCLL").trim();
		String requestedForMCL = dataTable.getData("General_Data", "RequestedForMCL").trim();
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxDetailingPriority, timeOutInSeconds);
		driverUtil.waitFor(2000);
		
		driverUtil.clickButton(UI_RecordACall.btnSave, timeOutInSeconds);
		driverUtil.waitFor(4000);
		
		// 1st Prescriber
		if (userType.equalsIgnoreCase("firstUser")) {
			driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
			driverUtil.clickLink(UI_RecordACall.lnkAttendees1, timeOutInSeconds);
			driverUtil.waitFor(2000);
			errorVerification("DetailingPriorityErrorMsg");
		}
		else {
			driverUtil.clickButton(UI_RecordACall.btnSubmit, timeOutInSeconds);
			driverUtil.clickLink(UI_RecordACall.lnkAttendees2, timeOutInSeconds);
			driverUtil.waitFor(2000);
			errorVerification("DetailingPriorityErrorMsg");
		}
		
		WebElement selectDetailingPriority = driver.findElement(By.xpath(
				"//*[@id='veeva-app']/div/div/form/div/div[2]/span[3]/div/div/div/div[2]/div/span/div/div/span/span/span/span/span/div/table/tbody/tr/td/table[1]/tbody[2]/tr[2]/td[1]/label/strong"));
		String dataToVerify_RecordStatus_DetailingPriority = selectDetailingPriority.getAttribute("outerHTML");
		if (dataToVerify_RecordStatus_DetailingPriority.contains("UNBRANDED CLL")) {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has been updated in the Key Focus Area", Status.PASS);
		} else {
			report.updateTestLog("Detailing Priority",
					"Selected detailing priority details has not been completely updated in the Key Focus Area",
					Status.PASS);
		}

		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxKeyFocusAreas, timeOutInSeconds);
		driverUtil.waitFor(2000);
		driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxOfferedSample, timeOutInSeconds);
		driverUtil.waitFor(2000);

		if (requestedForCLL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForCLL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

		if (requestedForMCL.equalsIgnoreCase("Yes")) {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRequestForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		} else {
			driverUtil.clickMultiSelectListBox(UI_RecordACall.chkboxRefuseForMCL, timeOutInSeconds);
			driverUtil.waitFor(2000);
		}

	}

	/**
	 * @Method errorVerification
	 * @author Cognizant
	 * @param none
	 * @return none
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void errorVerification(String columnname) throws NoSuchElementException, Throwable {
		
		String callReportErrorMsg = dataTable.getData("General_Data", columnname).trim();
		if(columnname.equalsIgnoreCase("DetailingPriorityErrorMsg")) {
			callReportErrorMsg = callReportErrorMsg + ":UNBRANDED CLL";
		}
		WebElement errorMessage = driver
				.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/div/div"));
		String errorMessage_CallReport = errorMessage.getText().trim();

		if (errorMessage_CallReport.equalsIgnoreCase(callReportErrorMsg)) {
			report.updateTestLog("Error Message Verification - " + columnname,
					"Verified successfully the error message in " + columnname + " section ", Status.PASS);
		} else {
			report.updateTestLog("Error Message Verification - " + columnname,
					"Verification failed for the error message in " + columnname + " section", Status.FAIL);
		}
	}

	/**
	 * @Method loginAsRoleSpecificUser
	 * @author Cognizant
	 * @param none
	 * @return none
	 * @throws InterruptedException
	 * @date 13-May-2019
	 */
	public void searchForUser(String strUser) throws NoSuchElementException, Throwable {

		driverUtil.clickButton(UI_RecordACall.btnHome, timeOutInSeconds);
		driverUtil.enterText(UI_RecordACall.txtSearchBox, timeOutInSeconds, strUser);
		driverUtil.clickButton(UI_RecordACall.btnSearchGo, timeOutInSeconds);
		driverUtil.waitFor(2000);
	}

}
