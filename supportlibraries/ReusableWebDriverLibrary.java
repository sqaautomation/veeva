package supportlibraries;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Class for storing general purpose business components
 * @author Cognizant
 */
public class ReusableWebDriverLibrary extends ReusableLibrary {
		
	/**
	 * Constructor to initialize the component library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public ReusableWebDriverLibrary(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	
	/**
	 * Function is used to select dropdown using visible text
	 * @author Tabasum
	 */
	
	public void selectUsingVisibleText(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByVisibleText(value);
		
	}
	
	/**
	 * Function is used to get all the options for the dropdown field
	 * @author Tabasum
	 * @return 
	 */
	
	public List<WebElement> getSelectedOption(By by, long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		List<WebElement> options = oselectlistboxvalue.getOptions();
		return options;
		
	}
	
	
	/**
	 * Function is used to select dropdown using value attribute
	 * @author Tabasum
	 */
	
	public void selectUsingValue(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
		
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByValue(value);
	
	}
	
	/**
	 * Function is used to get the selected value from dropdown
	 * @author Tabasum
	 */
	
	public String getSelectedListItem(By by) {

		Select select = new Select(driver.findElement(by));
		String option = select.getFirstSelectedOption().getText();
		return option;
	}
	
	/**
	 * Function is used to get the current date
	 * @author Tabasum
	 */
	public void deselectall(By by) {

		Select select = new Select(driver.findElement(by));
		select.deselectAll();
	}
	
public void selectUsingVisibleIndex(By by, long timeOutInSeconds, int n) {
		
		driverUtil.waitUntilElementEnabled(by, timeOutInSeconds);
		driverUtil.waitUntilElementVisible(by, timeOutInSeconds);
		WebElement elelistbox = driver.findElement(by);
		Select oselectlistboxvalue = new Select(elelistbox);
		oselectlistboxvalue.selectByIndex(n);
		
	}
	public String getCurrentDate(){
		
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		Date date = new Date();
		String cur_Date = dateFormat.format(date).toString();
		return cur_Date;
	}
	
	/**
	 * Function is used to get the text from dropdown
	 * @author Tabasum
	 */
	
	public String getText(By by,long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.visibilityOfElementLocated(by));
		
		String text = driver.findElement(by).getText().trim();
		return text;
	}
	
	public String getValue(By by,long timeOutInSeconds) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.visibilityOfElementLocated(by));
		
		String value = driver.findElement(by).getAttribute("value").trim();
		return value;
	}
	
	public void enterText(By by, long timeOutInSeconds, String value) {
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
		
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(value);
	}
	
	public void clickButton(By by, long timeOutInSeconds)
	{
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();	
	}

	public void clickLink(By by, long timeOutInSeconds){
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
	}
	
	public void clickMultiSelectListBox(By by, long timeOutInSeconds) {

		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by)).click();
	}
	
	public void waitForGetResultSet(By by, long timeOutInSeconds){
		
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	
	public boolean objectEnabled(By by, long timeOutInSeconds)
	{
		(new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
		.until(ExpectedConditions.elementToBeClickable(by));
				
		boolean statusenabled = driver.findElement(by).isEnabled();
		return statusenabled;
		
	}
	
	public boolean objectDisplayed(By by, long timeOutInSeconds)
	{
		return (new WebDriverWait(driver.getWebDriver(), timeOutInSeconds))
				.until(ExpectedConditions.elementToBeClickable(by)).isDisplayed();
	} 


}