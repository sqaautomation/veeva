Dim summaryFilePath,workspacePath,mailto,mailCC,subject,body,fso,project,executedby,browser,env,executiontime,totaltests,tfail,tpass


summaryFilePath = Wscript.Arguments.Item(0)
workspacePath = Wscript.Arguments.Item(1)

Set fso = CreateObject("Scripting.FileSystemObject")

Dim objMyFile: Set objMyFile =_
                              fso.OpenTextFile(workspacePath & "\utilityRun\EmailUtility.vbs", 1) ' 1 - For Reading
          
		Execute objMyFile.ReadAll()
          	
		Set objMyFile = Nothing

          	Dim objEmailUtility: Set objEmailUtility = New EmailUtility
          	
		mailto = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","To_Mail")

		mailCC = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","CC_Mail")

		subject = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","Summary_Subject")

		project = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","ProjectName")
		
		executedby = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","Executed_By")
		
		browser = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","DefaultBrowser")
		
		env = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global Settings.properties","Environment")
		
		executiontime = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global.properties","Execution_time")
		tpass = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global.properties","Test_Passed")
		tfail = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global.properties","Test_Failed")
		totaltests = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global.properties","Total_Tests")
		datetime = objEmailUtility.ReadVariablefromProperties(workspacePath+"\Global.properties","DateandTime")

body="<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head><body><div><div>Hello Team,</div><br/><div></div>"& project &" test execution"&_
" is completed.  Below is the quick tabular representation of execution status : </div></br>"&_
"<br/><table><tr ><td >Executed by</td><td>"&executedby&"</td></tr><tr ><td>Environment</td><td>"&env&"</td></tr><tr><td >Browser</td><td>"&browser&"</td></tr>"&_
"<tr><td>Executed on </td><td>"&datetime&"</td></tr><tr><td >Total Test Cases </td><td>"&totaltests&"</td></tr><tr><td >Passed </td><td>"&tpass&"</td></tr>"&_
"<tr><td >Failed </td><td>"&tfail&"</td></tr>"&_
"<tr><td>Total Duration</td><td>"&executiontime&"</td></tr></table></br></br><br/><div>Thanks,</div><div>QAE Automation</div></body></html>"

		objEmailUtility.SendMail mailto,mailCC,subject,body,summaryFilePath

Set fso = Nothing
