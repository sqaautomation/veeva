Class EmailUtility
	
	Private overAllSummaryFile,reportFiles,resultsPath,totalTests,passCount,failCount,notCompleted,noRun,others,strHostName,strRunStartTime,strRunEndTime
	Private Totalduration, totalVMSUsed,Status,m_objCurrentTestSet,m_strRelativePath,m_strUserName,vmsused
	
	'###################################################################################################################
	Public Property Let RelativePath(strRelativePath)
		m_strRelativePath = strRelativePath
	End Property
	'###################################################################################################################

	'###################################################################################################################
	Public Property Let UserName(strUserName)
		m_strUserName = strUserName 
	End Property
	'###################################################################################################################
	
	'###################################################################################################################
	Public Property Set CurrentTestSet(objCurrentTestSet)
		Set m_objCurrentTestSet = objCurrentTestSet
	End Property
	'###################################################################################################################


'#####################################################################################################################################################################
'Function to Capture Test results from ALM and Send via Outlook
'#####################################################################################################################################################################

public Function SendTestResults()

GetTestDetailsFromTestSet m_objCurrentTestSet

Dim mailto,subject,reportsUploadpath,environment,browser,project

mailto = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","To_Mail")

mailCC = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","CC_Mail")

subject = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","Summary_Subject")

environment = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","Summary_Environment")

browser = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","Summary_Browser")

project = ReadVariablefromProperties(m_strRelativePath+"\Global Settings.properties","ProjectName")

reportsUploadpath="<p>Reports can be found at :"&m_objCurrentTestSet.Name&"</p>"

body="<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style></head><body><div><div>Hello Team,</br></div></br><div></div>"& project &" test execution"&_
" is completed. Below is the quick tabular representation of execution status : </div></br>"&_
"<table><tr ><td >Executed by</td><td>"&m_strUserName&"</td></tr><tr ><td>Environment</td><td>"&environment&"</td></tr><tr><td >Browser</td><td>"&browser&"</td></tr>"&_
"<tr><td>Executed on </td><td>"&Date&"</td></tr><tr><td >Total TC's #</td><td>"&totalTests&"</td></tr><tr><td >Passed #</td><td>"&passCount&"</td></tr>"&_
"<tr><td >Failed #</td><td>"&failCount&"</td></tr><tr><td>Not Completed </td><td>"&notCompleted&"</td></tr><tr>"&_
"<td>No Run</td><td>"&noRun&"</td></tr><tr><td>Start Time</td><td>"&strRunStartTime&"</td></tr><tr><td>End Time</td><td>"&strRunEndTime&"</td></tr>"&_
"<tr><td>Total Duration</td><td>"&calculateTime(Totalduration)&"</td></tr></table></br><p>ALM test set Path       : "&m_objCurrentTestSet.TestSetFolder.Path&"\"&"</p>"&reportsUploadpath&"<p>VDIs used :</p>"&vmsused&"</p> </br><div>Thanks,</div><div>QAE Automation</div></body></html>"
  
SendTestResults = SendMail(mailto,mailCC,subject,body,reportFiles)

End Function

'#####################################################################################################################################################################
'Function to Capture Test results from ALM
'#####################################################################################################################################################################

Function GetTestDetailsFromTestSet(CurrentTestSet)

Dim strTestResults,tsTestFac,tsTestList,strTestListCount,tsRunFac,tsRunList,testvm,strRunID,strTestName,testduration,strTestStatus,sNo

overAllSummaryFile = m_strRelativePath & "\utilityRun\" & CurrentTestSet.Name & ".html"

Set objFso = CreateObject("Scripting.FileSystemObject")

if objFso.FileExists(m_strRelativePath & "\utilityRun\" & CurrentTestSet.Name & ".html") then
            objFso.DeleteFile m_strRelativePath & "\utilityRun\" & CurrentTestSet.Name & ".html",true
end if

if objFso.FileExists(m_strRelativePath & "\utilityRun\" & CurrentTestSet.Name & "_RegressionExecution.pdf") then
            objFso.DeleteFile m_strRelativePath & "\utilityRun\" & CurrentTestSet.Name & "_RegressionExecution.pdf",true
end if

strTestResults=""

StartTime = true

totalTests=0

passCount=0

failCount=0

testduration=0

noRun = 0

notCompleted = 0

sNo = 1

testvm=""

vmsused=""

Set tsTestFac = CurrentTestSet.TSTestFactory
            
Set tsTestList = tsTestFac.NewList("")

strTestListCount = tsTestList.Count

If strTestListCount > 0 Then

	For i = 1 To strTestListCount-1
                
                Set tsRunFac = tsTestList.Item(i).RunFactory

                Set tsRunList = tsRunFac.NewList("")

		strRunListCount = tsRunList.Count

		if strRunListCount > 0 then

			strRunID = tsRunList.Item(strRunListCount).ID

			strTestName = tsTestList.Item(i).TestName

			testduration = tsRunList.Item(strRunListCount).Field("RN_Duration")

			testvm = tsRunList.Item(strRunListCount).Field("RN_HOST")

			strTestStatus = tsRunList.Item(strRunListCount).Field("RN_STATUS")

			strRunEndTime = tsRunList.Item(strRunListCount).Field("RN_EXECUTION_TIME")

			DateRunEndTime = CDate(strRunEndTime)

			if StartTime = true then

				strRunStartTime = DateAdd("n",-testduration,DateRunEndTime)

				StartTime = False

			End IF

			If Len(strHostName) = 0 then 
		
				strHostName = testvm
			
			Else
		
				strHostName = strHostName&","&testvm

			End If

			Totalduration = DateDiff("n",strRunStartTime,DateRunEndTime)
		
		Else
			strTestName = tsTestList.Item(i).TestName
			
			strTestStatus = "No Run"

		End If

		
		If StrComp(strTestStatus,"Passed",1)=0 Then
						
			strTestStatus="<td style='color:green'>"&strTestStatus&"</td>"
		
		ElseIf StrComp(strTestStatus,"Failed",1)=0 Then

			strTestStatus="<td style='color:red'>"&strTestStatus&"</td>"
		
		Else
		
			strTestStatus="<td >"&strTestStatus&"</td>"
		
		End IF
		
		strTestResults = strTestResults& "<tr class='content' style='font-size:10px'><td>" & sNo & "</td><td>" & strTestName & "</td>" & strTestStatus &"</tr>"
					
		sNo = sNo + 1
		
		totalTests=totalTests+1

		If Instr(strTestStatus,"Passed") then
		
			passCount=passCount+1

		elseIf Instr(strTestStatus,"Failed") then

			failCount=failCount+1
					
		elseIf Instr(strTestStatus,"No Run") then
		
			noRun=noRun+1

		else
	
			notCompleted=notCompleted+1	
			
		End If

	Next

arrHostName = Split(strHostName, ",")

totalVMSUsed=0

For intLoop = 0 To UBound(arrHostName)

        If InStr( ucase(vmsused), ucase(arrHostName(intLoop))) = 0 Then

 		vmsused = arrHostName(intLoop) & ",</br>" & vmsused
          
		totalVMSUsed=totalVMSUsed+1

	End If

Next

fnHTMLCreation overAllSummaryFile

Set objMyFile = objFso.OpenTextFile(overAllSummaryFile, 8)

objMyFile.WriteLine ("</table><table><tr ><td style='border-left:1px solid;width:20%;;font-size:10px'>Total Tests:"&totalTests&"</td><td style='border-left:1px solid;width:54%;font-size:10px'> Passed :"&passCount&"</td><td style='font-size:10px'>Failed :"&failCount&"</td></tr></table><table style='text-align: Left; ' id='header'>" & _
                                   "<thead>" & _                                  
                                 "<tr class='heading'><th style='width:20%;font-size:10px'>S.No</th><th style='width:54%;font-size:10px'>Test Name</th><th style='font-size:10px'>Status</th></tr>"&strTestResults&"</table> </body></html>")
    
objMyFile.Close

resultsPath = m_strRelativePath&"\utilityRun"

fnPDFConversion overAllSummaryFile, resultsPath,m_objCurrentTestSet.Name

End If
        
    'Set nothing to the created objects
    Set tsTestFac = Nothing
    Set tsTestList = Nothing
    Set tsRunFac = Nothing
    Set tsRunList = Nothing
    Set objMyFile = Nothing
    Set objFso = Nothing

End Function


'#####################################################################################################################################################################
'Function to Create HTML File
'#####################################################################################################################################################################

Sub fnHTMLCreation(overAllSummaryFile)
    Dim objMyFile
    Set objFso = CreateObject("Scripting.FileSystemObject")
    Set objMyFile = objFso.CreateTextFile(overAllSummaryFile, True)
    objMyFile.Close
    
    'Initial Value pls don't change it
    intLoop = 1
    strTestSetName = ""
    Set objSysInfo = CreateObject( "WinNTSystemInfo" )
    strComputerName = objSysInfo.ComputerName
    Set objMyFile = objFso.OpenTextFile(overAllSummaryFile, 8)  ' 8 - Append Mode
    Dim strTestLogHeader
    strTestLogHeader = "<!DOCTYPE html><html><head><meta charset='UTF-8'><title>TFS Automation Execution Results</title>" & _
	GetThemeCss() & _
    GetJavascriptFunctions() & _
    "<table id='header' style='text-align: left;font-size:10px'><tr ><td style='width:20%'>Date & Time </td><td style='width:41%'>" & Now &_ 
"</td><td style='width:25%'>No of VM's</td><td style='width:14%'>"&totalVMSUsed&"</td></tr><tr ><td style='border:none;border-left:1px solid;border-bottom:1px solid;'>Test Set Path  </td><td style='border:none;border-left:1px solid;border-bottom:1px solid'>" & m_objCurrentTestSet.TestSetFolder.Path&"\"&m_objCurrentTestSet.Name & "</td></tr></table>" & _
                            "</head>" & _
                             "<body style='border: 1px solid #000000;'>"  
                                  
    objMyFile.WriteLine (strTestLogHeader)
    objMyFile.Close

    Set objMyFile = Nothing
    Set objFso = Nothing
    Set objSysInfo = Nothing

End Sub


'#####################################################################################################################################################################
'Function to get CSS Theme
'#####################################################################################################################################################################

Function GetThemeCss()
        Dim strThemeCss
        strThemeCss = "<style type='text/css'>body{font-family: Verdana, Geneva, sans-serif;" & _
                        "text-align: center;" & _
                        "border: 1px solid #000000;" & _
                        "}small {font-size: 0.7em;}" & _
                      "table {border: 1px solid #000000;width: 100%;text-align: left;" & _
                        "border-collapse: collapse;" & _
                        "border-spacing: 1px;" & _
                        "width: 100%;" & _
                        "margin-left: auto;" & _
                        "margin-right: auto;}"
            strThemeCss = strThemeCss & "tr.heading {" & _
                            "" & _
                            "border: 1px solid #000000;" & _
                            "font-weight: bold;text-align: center;}" & _
                           "tr.subheading {" & _
                            "border: 1px solid #000000;" & _
                            "color: #000000 " & ";" & _
                            "font-weight: bold;" & _
                            "font-size: 0.9em;" & _
                            "text-align: justify;}"
               strThemeCss = strThemeCss & "tr.section{" & _
                            "border: 1px solid #000000;color:#000000;cursor: pointer;" & _
                            "font-weight: bold;font-size: 0.9em;text-align: justify;}" & _
                           "tr.subsection {cursor: pointer;}" & _
                           "tr.content {color:#000000;font-size: 0.9em;" & _
                            "display: table-row;border: 1px solid #000000;}" & _
                           "td {padding: 4px;border: 1px solid #000000;text-align:inherit;" & _
                            "word-wrap: break-word;" & _
                            "max-width: 450px;}" & _
                           "th {padding: 4px;border: 1px solid #000000;text-align:inherit;" & _
                            "word-break: break-all;" & _
                            "max-width: 450px;}td.justified {" & _
                            "text-align: Left;" & _
                           "}td.pass {font-weight: bold;color: green;}"
                strThemeCss = strThemeCss & "td.fail {font-weight: bold;color: red;}" & _
                           "td.screenshot {font-weight: bold;color: navy;}" & _
                           "td.done {font-weight: bold;color: black;}" & _
                            "td.debug {" & _
                            "font-weight: bold;" & _
                            "color: blue;" & _
                           "}td.warning {font-weight: bold;color: orange;}</style>"
        
        GetThemeCss = strThemeCss

End Function


'#####################################################################################################################################################################
'Function to get Javascript Functions
'#####################################################################################################################################################################

Function GetJavascriptFunctions()
    Dim strJavascriptFunctions
    strJavascriptFunctions = "<script>function toggleMenu(objID) {if (!document.getElementById) return;" & _
                                "var ob = document.getElementById(objID).style;" & _
                                "if(ob.display === 'none') {try {" & _
                                      "ob.display='table-row-group';" & _
                                     "} catch(ex) {ob.display='block';}" & _
                                    "}else {" & _
                                     "ob.display='none';" & _
                                    "}}function toggleSubMenu(objId) {" & _
                                    "for(i=1; i<10000; i++) {" & _
                                     "var ob = document.getElementById(objId.concat(i));" & _
                                     "if(ob === null) {" & _
                                      "break;}" & _
                                     "if(ob.style.display === 'none') {" & _
                                      "try { " & _
                                       "ob.style.display='table-row';" & _
                                      "} catch(ex) {" & _
                                       "ob.style.display='block';" & _
                                      "}" & _
                                     "}" & _
                                     "else {" & _
                                      "ob.style.display='none';" & _
                                     "}" & _
                                    "}" & _
                                   "}" & _
                                  "</script>"
    
    GetJavascriptFunctions = strJavascriptFunctions
End Function


'#####################################################################################################################################################################
'Function to send Mail
'#####################################################################################################################################################################

public Function SendMail(SendTo,CC, Subject, Body, Attachment)

    Set ol=CreateObject("Outlook.Application")

    Set ObjMail=ol.CreateItem(0)

    ObjMail.to=SendTo

    ObjMail.Subject=Subject

    ObjMail.CC = CC	
  
    ObjMail.HTMLBody=Body

    a=Split(Attachment,";")

    for each x in a

      If (Attachment <> "") Then
         ObjMail.Attachments.Add(x)
      End If

    Next
    
    ObjMail.Send

    If Err Then

	   WScript.Echo "SendMail Failed:" & Err.Description

	   Status = "Failed"
    Else

	   Status = "Passed"
	
    End IF

    'ol.Quit
    Set ObjMail = Nothing
    Set ol = Nothing

    SendMail = Status

End Function

Function calculateTime(SecondsDifference)
Dim hours, minutes, seconds

  ' calculates whole hours (like a div operator)
  hours = SecondsDifference \ 60

  ' calculates the remaining number of seconds
  minutes  = SecondsDifference Mod 60

  ' calculates the whole number of minutes in the remaining number of seconds
  'minutes = SecondsDifference \ 60

  ' calculates the remaining number of seconds after taking the number of minutes
  seconds = 0
'hms = TimeSpan.FromSeconds(SecondsDifference)
'h = hms.Hours.ToString
'm = hms.Minutes.ToString
's = hms.Seconds.ToString
'Hours=Hours.ToString.PadLeft(2, "0"c)
'Minutes=Minutes.ToString.PadLeft(2, "0"c)
'Seconds=Seconds.ToString.PadLeft(2, "0"c)

calculateTime = hours & ":" & minutes & ":" & seconds

End Function


'#####################################################################################################################################################################
'Function to Convert Word to PDF
'#####################################################################################################################################################################

Sub fnPDFConversion(overAllSummaryFile, resultsPath,fileName)

'Kill_32BitProcess "Winword.exe"

	Dim filesys

	set filesys=CreateObject("Scripting.FileSystemObject")

	dim doccopy

	doccopy=resultsPath&"\summary1.docx"

	If filesys.FileExists(resultsPath&"\summary.docx") Then
	
		filesys.CopyFile resultsPath&"\summary.docx",doccopy 
	
	End If

	
	Set objWord = CreateObject( "Word.Application" )
	objWord.Visible = False


	Set objDocument = objWord.Documents.Open( resultsPath &"\"& fileName&".html",ConfirmConversions=True, Format=wdOpenFormatAuto)
	Set objSelection= objWord.Selection
	Dim strsrcpgnumber
	strsrcpgnumber=objDocument.ComputeStatistics(2)


	Set objWord1 = CreateObject("Word.Application")
	objWord1.visible=False
	Set objDocument1 = objWord.Documents.open(doccopy)
	Set objSelection1= objWord.Selection
         
	objSelection.wholestory
	objSelection.copy
	objSelection1.PasteAndFormat(wdPasteDefault)

	objDocument1.SaveAs resultsPath&"\"&fileName&"_RegressionExecution.pdf",17

	If (reportFiles <> "") Then
		reportFiles=reportFiles&";"&resultsPath&"\"&fileName&"_RegressionExecution.pdf"
	else
		reportFiles=resultsPath&"\"&fileName&"_RegressionExecution.pdf"
	End if

	objword.Application.Quit wdDoNotSaveChanges
	objWord1.Application.Quit wdDoNotSaveChanges

	set objword=Nothing
	Set objWord1 = Nothing
	Set filesys = Nothing
	Set objDocument = Nothing

'Kill_32BitProcess "Winword.exe"

End Sub


'#####################################################################################################################################################################
'Function to Kill Process
'#####################################################################################################################################################################

Function Kill_32BitProcess(ByVal strProcessToKill)    
    ' Variable Declaration
    Dim strComputer, objWMIService, colProcess, intCounter, objProcess
    Dim colProcess1
    strComputer = "."
    'strProcessToKill = "iexplore.exe" 
    'wait(1)
    Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2") 
    Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = '" & strProcessToKill & "'")
    
    intCounter = 0
    For Each objProcess in colProcess
        'wait(1)
        Set colProcess1 = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = '" & strProcessToKill & "'")                   
            If colProcess1.count = 0 Then
    Exit For
            Else
                objProcess.Terminate()
            End If
    'wait(1)
    Next 
End Function

'#####################################################################################################################################################################
'Function to Read Value from Properties File
'#############################################################################

public Function ReadVariablefromProperties(PropertyFilePath,strvariable)

set fso = CreateObject("Scripting.FileSystemObject")

SET fConFile = fso.OpenTextFile(PropertyFilePath)

do while(NOT fConFile.AtEndOfStream)

strConfigLine = fConFile.ReadLine

strConfigLine = TRIM(strConfigLine)

IF ((INSTR(1,strConfigLine,"#",1) <> 1) AND (INSTR(1,strConfigLine,strvariable,1) <> 0))THEN
EqualSignPosition = INSTR(1,strConfigLine,"=",1)

strLen = LEN(strConfigLine)

VariableValue = TRIM(Mid(strConfigLine, EqualSignPosition + 1, strLen - EqualSignPosition))

ReadVariablefromProperties= VariableValue

Exit do

End If

Loop

Set fso = nothing
Set fConFile = nothing

End Function


End Class

